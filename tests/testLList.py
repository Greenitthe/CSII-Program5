import sys
import unittest

sys.path.insert(0, '..')
from LList import *

class MainTest(unittest.TestCase):   
    
    def testReverseIter(self):
        aList = LList()
        for val in range(0,10):
            aList.append(val)
        goalVal = 9
        for val in aList.reverse_iter():
            self.assertEqual(goalVal, val)
            goalVal -= 1

def main(args):
    unittest.main()

if __name__ == '__main__':
    main(sys.argv)
